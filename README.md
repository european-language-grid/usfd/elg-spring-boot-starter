This project has moved
======================

The ELG Spring Boot Starter has moved to https://gitlab.com/european-language-grid/platform/elg-spring-boot-starter
